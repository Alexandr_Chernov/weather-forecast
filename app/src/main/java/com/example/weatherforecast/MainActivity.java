package com.example.weatherforecast;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private TextView forecast, under_forecast;
    private EditText search_city;
    private Button search_btn;
    private ListView list_forecast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        forecast = findViewById(R.id.forecast);
        under_forecast = findViewById(R.id.under_forecast);
        list_forecast = findViewById(R.id.list_forecast);

        search_city = findViewById(R.id.search_city);
        search_btn = findViewById(R.id.search_btn);

        search_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (search_city.getText().toString().trim().equals(""))
                    Toast.makeText(MainActivity.this, R.string.no_search_city_text, Toast.LENGTH_LONG).show();
                else {
                    String name_city = search_city.getText().toString();

                    String key = new Config().getAPI();
                    String url = "https://api.openweathermap.org/data/2.5/forecast?q=" + name_city + "&exclude=hourly,daily&units=metric&appid=" + key;
                    // https://api.openweathermap.org/data/2.5/weather?q=Moscow&exclude=hourly,daily&units=metric&appid=

                    new GetURLData().execute(url);
                }
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private class GetURLData extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            forecast.setText("Ожидание...");
            under_forecast.setText("");
        }

        @Override
        protected String doInBackground(String... strings) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(strings[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null)
                    buffer.append(line).append("\n");

                return buffer.toString();

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null)
                    connection.disconnect();
                try {
                    if (reader != null)
                        reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @SuppressLint("SetTextI18n")
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {

                if (result == null) {
                    Toast.makeText(MainActivity.this, R.string.error_city, Toast.LENGTH_LONG).show();
                    forecast.setText("");
                    under_forecast.setText("");
                } else {
                    JSONObject jsonObject = new JSONObject(result);

                    List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

                    for (int i = 0; i < 6; i++) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.DAY_OF_YEAR, i);
                        Date date = calendar.getTime();
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("MMdd");

                        Map<String, Object> map = new HashMap<String, Object>();

                        map.put("date", sdf.format(date)); // дата
                        map.put("week", getWeek(calendar.get(Calendar.DAY_OF_WEEK))); // день недели
                        map.put("temp", (int) jsonObject.getJSONArray("list").getJSONObject(i).getJSONObject("main").getDouble("temp")); // темп
                        map.put("temp_min", (int) Math.floor(jsonObject.getJSONArray("list").getJSONObject(i).getJSONObject("main").getDouble("temp_min"))); // мин. темп
                        map.put("temp_max", (int) Math.ceil(jsonObject.getJSONArray("list").getJSONObject(i).getJSONObject("main").getDouble("temp_max")));  // макс. темп

                        list.add(map);
                    }

                    forecast.setText(list.get(0).get("temp") + "°C");
                    under_forecast.setText(list.get(0).get("temp_min") + " / " + list.get(0).get("temp_max") + " °C");

                    String[] forecasts = new String[7];

                    forecasts[0] = "";
                    for (int j = 1; j < list.size(); j++) {
                        Map<String, Object> wMap = list.get(j);
                        if (j == 1)
                            forecasts[j] = "Завтра, " + getMonth((String) wMap.get("date")) + ". " + wMap.get("temp_min") + " / " + wMap.get("temp_max") + " °C";
                        else
                            forecasts[j] = wMap.get("week") + ", " + getMonth((String) wMap.get("date")) + ". " + wMap.get("temp_min") + " / " + wMap.get("temp_max") + " °C";
                    }
                    forecasts[6] = "";

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,
                            android.R.layout.simple_list_item_1,
                            forecasts) {
                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            View view = super.getView(position, convertView, parent);
                            TextView tv = (TextView) view.findViewById(android.R.id.text1);
                            tv.setTextColor(Color.WHITE);
                            return view;
                        }
                    };

                    list_forecast.setAdapter(adapter);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    private String getMonth(String MMdd) {
        String month = "";
        switch (MMdd.substring(0, 2)) {
            case "01":
                month = "янв";
                break;
            case "02":
                month = "фев";
                break;
            case "03":
                month = "март";
                break;
            case "04":
                month = "апр";
                break;
            case "05":
                month = "мая";
                break;
            case "06":
                month = "июн";
                break;
            case "07":
                month = "июл";
                break;
            case "08":
                month = "авг";
                break;
            case "09":
                month = "сен";
                break;
            case "10":
                month = "окт";
                break;
            case "11":
                month = "нояб";
                break;
            case "12":
                month = "дек";
                break;
            default:
                break;
        }
        return MMdd.substring(2) + " " + month;
    }

    private String getWeek(int n) {
        switch (n) {
            case 2:
                return "Пн";
            case 3:
                return "Вт";
            case 4:
                return "Ср";
            case 5:
                return "Чт";
            case 6:
                return "Пт";
            case 7:
                return "Сб";
            case 1:
                return "Вс";
            default:
                return "";
        }
    }

}
// извините за ListView